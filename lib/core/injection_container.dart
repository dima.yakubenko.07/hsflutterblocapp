import 'package:get_it/get_it.dart';
import 'package:hs_flutter_bloc_app/data/repositories/ari_repository.dart';
import 'package:hs_flutter_bloc_app/ui/cards_list_screen/hs_list_bloc/hs_list_bloc.dart';

GetIt locator = GetIt.instance;

Future<void> initDI() async {
  //Repositories
  locator.registerSingleton<ApiRepository>(ApiRepository());
  //Bloc
  locator.registerLazySingleton(
      () => HsListBloc(hsApiRepository: locator<ApiRepository>()));
}
