class AppStrings {
  //Api Repository
  static const String rapidApiHost = 'omgvamp-hearthstone-v1.p.rapidapi.com';
  static const String httpsAdd = 'https://';
  static const String contentType = 'application/json';
  static const String getCCfromApi = '/cards?collectible=1';
  static const String dioError = 'Failed to fetch cards list from API:';
  static const String otherNetworkError = 'Failed to fetch cards list:';

  //BloC
  static const String hsListError = 'Failed to get cards:';

  //Ui
  static const String somethingGotWrong = 'Something got wrong:';
  static const String unknownState = 'Unknown state';
  static const String cardListTitle = 'Hearthstone cards';
  static const String cardName = 'Ragnaros';

  //Images
  static const String cardContainerPath = 'assets/images/card_container.png';
}
