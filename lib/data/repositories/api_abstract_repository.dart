import '../models/hs_card_model.dart';

abstract class ApiAbstractRepository {
  Future<List<HsCardModel>> getCollectibleCardsFromApi();
}
