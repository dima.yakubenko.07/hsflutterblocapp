import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:hs_flutter_bloc_app/core/resources/app_strings.dart';
import 'package:hs_flutter_bloc_app/data/models/hs_card_model.dart';
import 'package:hs_flutter_bloc_app/data/repositories/api_abstract_repository.dart';

import '../models/api_response_model.dart';

class ApiRepository implements ApiAbstractRepository {
  static const _baseUrl = AppStrings.httpsAdd + AppStrings.rapidApiHost;

  final Dio _dio = Dio(BaseOptions(
    baseUrl: _baseUrl,
    headers: {
      'X-RapidAPI-Key': dotenv.env['API_KEY'],
      'X-RapidAPI-Host': AppStrings.rapidApiHost,
    },
    contentType: AppStrings.contentType,
  ));

  @override
  Future<List<HsCardModel>> getCollectibleCardsFromApi() async {
    try {
      final response = await _dio.get(AppStrings.getCCfromApi);
      final ApiResponseModel apiResponse =
          ApiResponseModel.fromJson(response.data);

      final List<HsCardModel> cardList = [];

      apiResponse.cardSets.forEach((setName, cards) {
        cards.forEach((card) {
          cardList.add(card);
        });
      });

      return cardList;
    } on DioException catch (e) {
      throw Exception('${AppStrings.dioError}$e');
    } catch (e) {
      throw Exception('${AppStrings.otherNetworkError}$e');
    }
  }
}
