import 'hs_card_model.dart';

class ApiResponseModel {
  final Map<String, List<HsCardModel>> cardSets;

  ApiResponseModel(this.cardSets);

  factory ApiResponseModel.fromJson(Map<String, dynamic> json) {
    final Map<String, List<HsCardModel>> cardSets = {};

    json.forEach((key, value) {
      if (value is List) {
        cardSets[key] = List<HsCardModel>.from(
            value.map((item) => HsCardModel.fromJson(item)));
      } else if (value is Map) {
        cardSets[key] = [];
      }
    });

    return ApiResponseModel(cardSets);
  }
}
