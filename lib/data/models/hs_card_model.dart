class HsCardModel {
  HsCardModel({
    required this.cardId,
    required this.dbfId,
    required this.name,
    required this.cardSet,
    required this.type,
    required this.rarity,
    required this.cost,
    required this.attack,
    required this.health,
    required this.text,
    required this.flavor,
    required this.artist,
    required this.collectible,
    required this.race,
    required this.playerClass,
    required this.howToGet,
    required this.howToGetGold,
    required this.img,
    required this.locale,
    required this.mechanics,
  });

  factory HsCardModel.fromJson(Map<String, dynamic> json) {
    List<Mechanics> mechanicsList = [];

    if (json['mechanics'] != null && json['mechanics'] is List) {
      mechanicsList = List<Mechanics>.from(
          json['mechanics'].map((x) => Mechanics.fromJson(x)));
    } else {
      mechanicsList = [];
    }
    return HsCardModel(
      cardId: json['cardId'],
      dbfId: json['dbfId'],
      name: json['name'],
      cardSet: json['cardSet'],
      type: json['type'],
      rarity: json['rarity'],
      cost: json['cost'],
      attack: json['attack'],
      health: json['health'],
      text: json['text'],
      flavor: json['flavor'],
      artist: json['artist'],
      collectible: json['collectible'],
      race: json['race'],
      playerClass: json['playerClass'],
      howToGet: json['howToGet'],
      howToGetGold: json['howToGetGold'],
      img: json['img'],
      locale: json['locale'],
      mechanics: mechanicsList,
    );
  }

  final String cardId;
  final num dbfId;
  final String? name;
  final String? cardSet;
  final String? type;
  final String? rarity;
  final num? cost;
  final num? attack;
  final num? health;
  final String? text;
  final String? flavor;
  final String? artist;
  final bool? collectible;
  final String? race;
  final String? playerClass;
  final String? howToGet;
  final String? howToGetGold;
  final String? img;
  final String? locale;
  final List<Mechanics>? mechanics;

  Map<String, dynamic> toJson() {
    return {
      'cardId': cardId,
      'dbfId': dbfId,
      'name': name,
      'cardSet': cardSet,
      'type': type,
      'rarity': rarity,
      'cost': cost,
      'attack': attack,
      'health': health,
      'text': text,
      'flavor': flavor,
      'artist': artist,
      'collectible': collectible,
      'race': race,
      'playerClass': playerClass,
      'howToGet': howToGet,
      'howToGetGold': howToGetGold,
      'img': img,
      'locale': locale,
      'mechanics':
          List<dynamic>.from(mechanics?.map((x) => x.toJson()) ?? List.empty()),
    };
  }
}

class Mechanics {
  Mechanics({
    required this.name,
  });

  factory Mechanics.fromJson(Map<String, dynamic> json) {
    return Mechanics(
      name: json['name'],
    );
  }

  final String name;

  Map<String, dynamic> toJson() {
    return {
      'name': name,
    };
  }
}
