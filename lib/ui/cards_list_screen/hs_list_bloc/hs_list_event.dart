part of 'hs_list_bloc.dart';

@immutable
abstract class HsListEvent extends Equatable {
  @override
  List<Object?> get props => [];
}

class LoadCardsEvent extends HsListEvent {}
