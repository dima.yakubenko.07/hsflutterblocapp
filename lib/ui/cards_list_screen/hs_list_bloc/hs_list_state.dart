part of 'hs_list_bloc.dart';

@immutable
abstract class HsListState extends Equatable {
  @override
  List<Object?> get props => [];
}

class HsListInitial extends HsListState {}

class HsListIsLoading extends HsListState {}

class HsListLoaded extends HsListState {
  final List<HsCardModel> cardsList;

  @override
  List<Object?> get props => [cardsList];

  @override
  HsListLoaded(this.cardsList);
}

class HsListError extends HsListState {
  final String message;

  @override
  List<Object?> get props => [message];

  HsListError(this.message);
}
