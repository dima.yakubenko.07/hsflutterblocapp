import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:hs_flutter_bloc_app/core/resources/app_strings.dart';
import 'package:hs_flutter_bloc_app/data/models/hs_card_model.dart';
import 'package:hs_flutter_bloc_app/data/repositories/ari_repository.dart';
import 'package:meta/meta.dart';

part 'hs_list_event.dart';

part 'hs_list_state.dart';

class HsListBloc extends Bloc<HsListEvent, HsListState> {
  final ApiRepository hsApiRepository;
  HsListBloc({required this.hsApiRepository}) : super(HsListInitial()) {
    on<LoadCardsEvent>((event, emit) async {
      await _load(event, emit);
    });
  }

  Future<void> _load(LoadCardsEvent event, Emitter<HsListState> emit) async {
    emit(HsListIsLoading());
    try {
      final cards = await hsApiRepository.getCollectibleCardsFromApi();
      emit(HsListLoaded(cards));
    } catch (error) {
      emit(HsListError('${AppStrings.hsListError} $error'));
    }
  }
}
