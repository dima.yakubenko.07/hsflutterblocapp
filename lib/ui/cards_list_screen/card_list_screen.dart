import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hs_flutter_bloc_app/core/resources/app_strings.dart';
import 'package:hs_flutter_bloc_app/ui/cards_list_screen/hs_list_bloc/hs_list_bloc.dart';

import '../../core/injection_container.dart';
import '../../data/models/hs_card_model.dart';

class CardListScreen extends StatefulWidget {
  const CardListScreen({super.key});

  @override
  State<CardListScreen> createState() => _CardListScreenState();
}

class _CardListScreenState extends State<CardListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(AppStrings.cardListTitle),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.filter_alt),
          )
        ],
      ),
      body: BlocBuilder<HsListBloc, HsListState>(
        bloc: locator.get<HsListBloc>()..add(LoadCardsEvent()),
        builder: (context, state) {
          if (state is HsListIsLoading) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is HsListLoaded) {
            final cardsList = state.cardsList;
            return _View(cards: cardsList);
          } else if (state is HsListError) {
            return Center(
              child: Text('${AppStrings.somethingGotWrong} ${state.message}'),
            );
          } else {
            return const Center(
              child: Text(AppStrings.unknownState),
            );
          }
        },
      ),
    );
  }
}

class _View extends StatefulWidget {
  final List<HsCardModel> cards;

  const _View({super.key, required this.cards});

  @override
  State<_View> createState() => _ViewState();
}

class _ViewState extends State<_View> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuerySize = MediaQuery.sizeOf(context);
    final height = mediaQuerySize.height * 0.2;
    final width = mediaQuerySize.width * 0.3;
    return GridView.builder(
      controller: _scrollController,
      gridDelegate:
          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemCount: widget.cards.length,
      itemBuilder: (context, index) {
        final card = widget.cards[index];
        return Card(
          // Customize the appearance of each card based on your needs
          child: Column(
            children: [
              SizedBox(
                height: height,
                width: width,
                child: card.img == null
                    ? Image.asset(AppStrings.cardContainerPath)
                    : Image.network(card.img ?? ''),
              ),
              Text(card.name ?? AppStrings.cardName),
            ],
          ),
        );
      },
    );
  }
}
